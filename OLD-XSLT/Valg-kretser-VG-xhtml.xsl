<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="html" omit-xml-declaration="no" indent="no" standalone="yes"/>

<xsl:param name="FylkeNr">00</xsl:param>
<xsl:param name="ByNr">0301</xsl:param>
<!--<xsl:variable name="Form">Default.aspx</xsl:variable>-->
<xsl:param name="xmlpathfylke">C:\Inetpub\wwwroot\Valget2003\xml-docs\F04-</xsl:param>
<xsl:param name="xmlpathfylke03">C:\Inetpub\wwwroot\Valget2003\xml-docs\K04-</xsl:param>
<xsl:param name="xmlpathkommune">C:\Inetpub\wwwroot\Valget2003\xml-docs\K02-</xsl:param>
<xsl:param name="xmlpathkrets">C:\Inetpub\wwwroot\Valget2003\xml-docs\K03.XML</xsl:param>

<xsl:template match="respons">
	<xsl:choose>
		<xsl:when test="$FylkeNr=0">
			<xsl:apply-templates select="rapport"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="rapport[data[@navn='FylkeNr']=$FylkeNr]"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="rapport">
	<xsl:variable name="path">
		<xsl:choose>
			<xsl:when test="data[@navn='FylkeNr']=03">
				<xsl:value-of select="concat($xmlpathfylke03, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($xmlpathfylke, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	
	<!-- Fylkestabell -->
	<tr>
	<td colspan="3">
	<table width="100%" border="1" cellpadding="3" cellspacing="0">
		<tr>
			<td rowspan="3" class="fylkegif" width="80" align="center">
				<xsl:attribute name="id">
					<xsl:value-of select="data[@navn='FylkeNr']"/>
				</xsl:attribute>
				<img border="0" width="45" height="55">
					<xsl:attribute name="src">Images\fylke-<xsl:value-of select="data[@navn='FylkeNr']"/>.gif</xsl:attribute>
				</img>
			</td>
			<td class="fylkehead" colspan="9" valign="middle" align="center"><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
		</tr>
	
		<tr>
			<td colspan="9">
			<xsl:text>Fremmøte: </xsl:text>
			<xsl:value-of select="document($path)/respons/rapport/data[@navn='ProFrammotte']"/>
			<xsl:text>%</xsl:text>
			</td>
		</tr>
	
		<xsl:call-template name="fylke">
			<xsl:with-param name="fylkenr">
				<xsl:value-of select="data[@navn='FylkeNr']"/>
			</xsl:with-param>
			<xsl:with-param name="path">
				<xsl:value-of select="$path"/>
			</xsl:with-param>
		</xsl:call-template>

	</table>
	</td>
	</tr>

	<!-- Forklaringstekst -->
	<tr class="valgtype">
		<td valign="bottom" align="center"><img border="0" src="images/down.gif"/>Kommunevalg</td>
		<td valign="top" align="center"><img border="0" src="images/up.gif"/>Fylkestingsvalg</td>
		<td valign="center" align="center">(Kl. <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>)</td>
	</tr>

	<!-- Kommunetabell -->
	<tr>
	<td colspan="3">
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
		
		<xsl:choose>
		<xsl:when test="data[@navn='FylkeNr']=03">
			<xsl:call-template name="kretshead"/>
			<xsl:for-each select="document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']='0301']">
				<tr>
					<td><xsl:value-of select="data[@navn='KretsNr']"/></td>
					<td><xsl:value-of select="data[@navn='KretsNavn']"/></td>

					<xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2]">
						<!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
						<td align="right">&#160;<xsl:value-of select="data[@navn='ProSt']"/></td>
					</xsl:for-each>
				</tr>
			</xsl:for-each>

		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="kommunehead"/>
			<xsl:apply-templates select="tabell/liste">
				<xsl:sort select="data[@navn='KommNavn']"/>
			</xsl:apply-templates>
		</xsl:otherwise>	
		</xsl:choose>
			
		</table>

	</td>
	</tr>
	</table>
	<br/>
</xsl:template>

<xsl:template name="fylke">
	<xsl:param name="fylkenr"></xsl:param>
	<xsl:param name="path"></xsl:param>

	<!--<xsl:value-of select="concat($xmlpathfylke, $fylkenr)"/>-->
	<tr>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<th><xsl:value-of select="data[@navn='Partikode']"/></th>
	</xsl:for-each>
	</tr>

	<tr>
	<td>Valg 2003</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right">&#160;<xsl:value-of select="data[@navn='ProSt']"/></td>
	</xsl:for-each>
	</tr>

	<tr>
	<td>2003 - 2001</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right">&#160;<xsl:value-of select="data[@navn='DiffPropFStv']"/></td>
	</xsl:for-each>
	</tr>

	<td>2003 - 1999</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:choose>
		<xsl:when test="$fylkenr=03">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
		</xsl:otherwise>
		</xsl:choose>
		</td>
	</xsl:for-each>

</xsl:template>


<xsl:template match="tabell/liste">
	<tr>
	<td><xsl:value-of select="data[@navn='KommNavn']"/></td>
	<xsl:call-template name="kommune">
		<xsl:with-param name="kommunenr">
		<xsl:value-of select="data[@navn='KommNr']"/>
		</xsl:with-param>
	</xsl:call-template>
	</tr>
</xsl:template>


<xsl:template name="kommune">
	<xsl:param name="kommunenr"></xsl:param>
	
	<xsl:variable name="path">
		<xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/>
	</xsl:variable>
	
	<!--
	<td><xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/></td>	
	-->
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='RV']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<xsl:text>&#160;</xsl:text>
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProSt']"/>
	</td>
</xsl:template>

<xsl:template name="kommunehead">
	<tr>
	<th>Kommune</th>
	<th>A</th>
	<th>SV</th>
	<th>RV</th>
	<th>Sp</th>
	<th>Krf</th>
	<th>V</th>
	<th>H</th>
	<th>FrP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="kretshead">
	<tr>
	<th>Kretsnr.</th>
	<th>Krets</th>
	<th>A</th>
	<th>SV</th>
	<th>RV</th>
<!--
	<th>Sp</th>
-->	
	<th>Krf</th>
	<th>V</th>
	<th>H</th>
	<th>FrP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

</xsl:stylesheet>